let truth_table literal_one literal_two expression = 
    let rec evaluate_expression bool_one bool_two = function
        | Lit(literal) -> if (literal = literal_one) then bool_one
                          else bool_two
        | Not(expr) -> not (evaluate_expression bool_one bool_two expr)
        | And(expr_one, expr_two) -> (evaluate_expression bool_one bool_two expr_one)
                                     && (evaluate_expression bool_one bool_two expr_two)
        | Or(expr_one, expr_two) -> (evaluate_expression bool_one bool_two expr_one)
                                     || (evaluate_expression bool_one bool_two expr_two)
    in [(true, true, evaluate_expression true true expression);
        (true, false, evaluate_expression true false expression);
        (false, true, evaluate_expression false true expression);
        (false, false, evaluate_expression false false expression)];;