type 'a stack =
    | Empty
    | Stack of 'a list;;

let start stack_func =
    stack_func Empty;;

let halt stack = match stack with
    | Empty -> []
    | Stack lst -> lst;;

let push n stack stack_func = match stack with
    | Empty -> stack_func (Stack([n]))
    | Stack lst -> stack_func (Stack(n::lst));;

let pop stack stack_func = match stack with
    | Empty -> stack_func Empty
    | Stack lst -> stack_func (Stack(List.tl lst));;

let add stack stack_func = match stack with
    | Empty -> stack_func Empty
    | Stack lst -> let added = ((List.hd lst) + (List.hd (pop stack halt))) in
                   let popped_stack = (Stack(pop (Stack(pop stack halt)) halt)) in
                   stack_func (Stack((push added) popped_stack halt));;

let mult stack stack_func = match stack with
    | Empty -> stack_func Empty
    | Stack lst -> let added = ((List.hd lst) * (List.hd (pop stack halt))) in
                   let popped_stack = (Stack(pop (Stack(pop stack halt)) halt)) in
                   stack_func (Stack((push added) popped_stack halt));;

let clone stack stack_func = match stack with
    | Empty -> stack_func Empty
    | Stack lst -> stack_func (Stack((push (List.hd lst)) stack halt));; 

let kpop stack stack_func = match stack with
    | Empty -> stack_func Empty
    | Stack lst -> let k = (List.hd lst) in
                   let rec aux n aux_stack = match aux_stack with
                        | Empty -> Empty
                        | Stack lst -> if n <= 0 then aux_stack
                                       else aux (n-1) (Stack(pop aux_stack halt))
                    in stack_func (aux k (Stack(pop stack halt)));;