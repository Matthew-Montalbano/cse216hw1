(* Question one *)
let rec pow x n =
    if n = 0 then 1
    else x * pow x (n - 1);;

let rec float_pow x n = 
    if n = 0 then 1.
    else x *. float_pow x (n - 1);;

(* Question two *)
let compress list =
    if list = [] then []
    else let rec aux prev_elem acc = function
        | [] -> acc
        | h::t -> if prev_elem = h then aux prev_elem acc t
                  else aux h (acc @ [h]) t
    in aux (List.hd list) ([List.hd list]) list;;

(* Question three *)
let remove_if list func =
    let rec aux acc = function
        | [] -> acc
        | h::t -> if func (h) then aux acc t
                  else aux (acc @ [h]) t
    in aux [] list;;

(* Question four *)
let slice list i j =
    let rec aux acc index aux_list = 
        if (index >= j || aux_list = []) then acc
        else if (index < i) then aux acc (index + 1) (List.tl aux_list)
        else aux (acc @ [List.hd aux_list]) (index + 1) (List.tl aux_list)
    in aux [] 0 list;;

(* Question five *)
let rec check_equiv_class acc not_in_equiv_class first_head equiv_func = function
    | [] -> (acc, not_in_equiv_class)
    | h::t -> if (equiv_func first_head h) then check_equiv_class (acc @ [h]) not_in_equiv_class first_head equiv_func t
              else check_equiv_class acc (not_in_equiv_class @ [h]) first_head equiv_func t;;

let equivs equiv_func list =
    let rec aux acc = function
        | [] -> acc
        | h::t -> let (in_equiv_class, not_in_equiv_class) = check_equiv_class [h] [] h equiv_func t
                  in aux (in_equiv_class::acc) not_in_equiv_class
    in aux [] list;;

(* Question six *)
let determine_if_prime n =
    if (n = 1) then false
    else let rec aux x = 
        if (x = n) then true
        else if (n mod x) = 0 then false
        else aux (x+1)
    in aux 2;; 

let goldbachpair sum = 
    let rec aux valueOne valueTwo =
        if (valueOne < valueTwo) then (0, 0)
        else if determine_if_prime valueOne && determine_if_prime valueTwo then (valueTwo, valueOne)
        else aux (valueOne-1) (valueTwo+1)
    in aux sum 0;;

(* Question seven *)
let equiv_on f g lst = 
    let rec aux  = function
        | [] -> true
        | h::t -> if ((f h) = (g h)) then aux t
                  else false
    in aux lst;;

(* Question eight *)
let pairwisefilter cmp lst = 
    let rec aux acc = function
        | [] -> acc
        | [a] -> (a::acc)
        | h::t -> aux ((cmp h (List.hd t))::acc) (List.tl t)
    in let filtered = aux [] lst 
    in (List.rev filtered);;

(* Question nine *)
let polynomial list_of_tuples = 
    let rec aux polynomial_func = function
        | [] -> polynomial_func
        | h::t -> let (coefficient, exponent) = h
                  in aux (fun x -> polynomial_func x + (coefficient * pow x exponent)) t
    in aux (fun x -> 0) list_of_tuples;;

let rec pow x n =
    if n = 0 then 1
    else x * pow x (n - 1);;

(* Question ten *)
let rec add_to_subsets value acc = function
    | [] -> acc
    | h::t -> add_to_subsets value ((h @ [value])::acc) t;;

let powerset lst = 
    let rec aux acc = function
        | [] -> acc
        | h::t -> aux ((add_to_subsets h [] acc) @ acc) t
    in (List.rev (aux [[]] lst));;
